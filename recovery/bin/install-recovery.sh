#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:32378480:2a9fdca492c7168118d80ebaac98297b8f91ecb1; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot:11294320:be015388773a922fdf6342e576c3c84067198500 EMMC:/dev/block/platform/bootdevice/by-name/recovery 2a9fdca492c7168118d80ebaac98297b8f91ecb1 32378480 be015388773a922fdf6342e576c3c84067198500:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
